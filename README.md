# ifml-font

A IFML icon font. Based on the [bpmn-font](https://github.com/bpmn-io/bpmn-font).

Use it to model IFML diagrams in your Word, VIM or other text editor.

![Font Preview](./resources/symbols.png)

Checkout the [demo page](https://metaaid.gitlab.io/ifml-io/ifml-font/demo.html) for a list of included icons and their character code.


## Usage in HTML

Expose [`dist`](./dist) directory in your app and include [`dist/css/ifml-font.css`](./dist/css/ifml-font.css) in a webpage.

Use icons prefixed with `icon-`, i.e.:

```html
<span class="icon-task"></span>
```


## License

[SIL](http://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)
